﻿'use strict';

const url = require('url');
const https = require('https');

if (process.argv[2]) {
	let params = process.argv.slice(2);
	let serverURL = params.join(' ');
	if (serverURL.includes('://')) serverURL = url.parse(serverURL).host;
	if (serverURL.slice(-1) === '/') serverURL = serverURL.slice(0, -1);
	
	console.log(`Getting data for ${serverURL}...\n`);

	https.get(`https://play.pokemonshowdown.com/crossdomain.php?host=${serverURL}&path=`, (res) => {
		res.setEncoding('utf8');

		if (res.statusCode !== 200) return console.error(`ERROR: Problem fetching data\n\n${err.stack}\n\n`);
		
		let data = "";
		res.on('data', (chunk) => {
			data += chunk;
		});

		res.on('error', (err) => {
			return console.error(`ERROR: Problem fetching data\n\n${err.stack}\n\n`);
		});

		res.on('end', () => {
			const searchStr = "var config = ";
			const startingIndex = data.indexOf(searchStr);

			let len = searchStr.length;
			if (!data.length) return console.error("ERROR: Failed to get data; invalid server url (example: azure.psim.us);");

			data = data.substr(startingIndex);
			data = JSON.parse(data.substr(len, (data.indexOf(";") - len)));

			while (typeof data === "string") data = JSON.parse(data);

			if (!data.registered) return console.error("ERROR: Must be a registered server");

			return console.log(`SERVER:\t${data.host}\nPORT:\t${data.port}\nID:\t${data.id}\n`);
		});
	});
} else {
	console.log("Why don't you try something like\n$ node getserver.js azure.psim.us");
}