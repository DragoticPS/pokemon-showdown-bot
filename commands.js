/**
 * This is the file where the bot commands are located
 *
 * @license MIT license
 */

exports.commands = {
	reload: function (arg, user, room) {
		if (!user.isExcepted()) return false;
		try {
			this.uncacheTree('./commands.js');
			Commands = require('./commands.js').commands;
			this.say(room, 'Commands reloaded.');
		} catch (e) {
			error('failed to reload: ' + e.stack);
		}
	},

	away: function (arg, user, room) {
		if (!user.isExcepted()) return false;
		if (!arg) return this.say(room, `${Config.commandcharacter}away enable | ${Config.commandcharacter}away [msg]`);
		if (toId(arg) !== "enable" && !Config.autoReply) return this.say(room, `Use "${Config.commandcharacter}away enable" to set an away message`);
		if (toId(arg) === "enable") {
			if (Config.autoReply) return this.say(room, `Away message is already enabled, use "${Config.commandcharacter}away disable" to disable away messages`);
			
			Config.autoReply = true;
			return this.say(room, `Away message enabled, use "${Config.commandcharacter}away [msg]" to set an away message`);
		} else if (toId(arg) === "disable") {
			if (!Config.autoReply) return this.say(room, `Away message is already disabled, use "${Config.commandcharacter}away enable" to enable away messages`);
			
			Config.autoReply = false;
			return this.say(room, `Away message disabled`);
		}
		
		Config.defaultMsg = arg.trim();
		return this.say(room, "Away message set!");
	},
};
